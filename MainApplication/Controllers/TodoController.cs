using Microsoft.AspNetCore.Mvc;

namespace MainApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodoController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<TodoController> _logger;

        public TodoController(ILogger<TodoController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetTodoItems")]
        public IEnumerable<TodoItem> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new TodoItem
            {
                Name = $"Deal N {index}",
                IsCompleted = false,
            })
            .ToArray();
        }
    }
}