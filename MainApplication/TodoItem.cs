namespace MainApplication
{
    public class TodoItem
    {
        public string? Name { get; set; }
        public bool IsCompleted { get; set; }
    }
}